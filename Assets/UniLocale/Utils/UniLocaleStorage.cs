﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;
using System.Xml.Linq;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Storage Class
//=====================================================
namespace UniLocale{
    /**
     * UniLocaleStorage
     */
    public static class UniLocaleStorage
    {
        /**
         * Save File to Path
         * @param string file_path Path to Save
         * @param string data Data to Save
         * @param bool isEncoded Encode data to Base64?
         * @returns bool Operation Status
         */
        public static bool saveFile(string file_path, string data = "", bool isEncoded = false){
            /* Encode Base64 File */
            if (isEncoded){
                byte[] _bytesToEncode = Encoding.UTF8.GetBytes(data);
                data = Convert.ToBase64String(_bytesToEncode);
            }

            /* Save File to Path */
            File.WriteAllText(file_path, data, Encoding.UTF8);
            UniLocaleSDK.Instance.Log("File completely saved to \"" + file_path + "\"");
            return true;
        }

        /**
         * Open File from Path
         * @param string file_path Path to File
         * @param bool isEncoded Decode data from Base64?
         * @returns string|null File Data
         */
        public static string openFile(string file_path, bool isEncoded = false){
            /* Trying to find File */
            if (!File.Exists(file_path)){
                UniLocaleSDK.Instance.Log("Failed to load file. File \"" + file_path + "\" not found");
                return null;
            }

            /* Load File */
            string _data = File.ReadAllText(file_path);
            if (isEncoded){
                byte[] _decodedBytes = Convert.FromBase64String(_data);
                string _decodedData = Encoding.UTF8.GetString(_decodedBytes);
                return _decodedData;
            }

            // Return Data
            return _data;
        }

        /**
         * Parse Locale Model and Returns Full Model
         * @param LocaleModel asset Locale Model with Unparset Values
         * @param UniStorageMode assetType Asset Type
         */
        public static LocaleModel parseLocaleModel(LocaleModel asset, UniStorageMode assetType){
            /* Prepare Locale Model */
            LocaleModel _locale = asset;
            _locale.languageValues = new List<LocaleModel.TextKeyValue>(); // Create Values List

            /* Parse XML */
            if(assetType == UniStorageMode.XMLFiles){
                XDocument languageXMLData = XDocument.Parse(asset.languageFile.text);
                foreach (XElement textx in languageXMLData.Element("Language").Elements()){
                    LocaleModel.TextKeyValue textKeyValue = new LocaleModel.TextKeyValue();
                    textKeyValue.key = textx.Attribute("key").Value;
                    textKeyValue.value = textx.Value;
                    _locale.languageValues.Add(textKeyValue);
                }

                /* Return Model with Parsed Values */
                _locale.languageFile = null; // Remove Text Asset
                return _locale;
            }

            /* Parse JSON */
            if (assetType == UniStorageMode.JSONFiles){
                _locale = JsonUtility.FromJson<LocaleModel>(asset.languageFile.text);
                _locale.languageFile = null; // Remove Text Asset
                return _locale;
            }

            /* Parse Multiline Text */
            if(assetType == UniStorageMode.MultilineText){
                string[] _lines = File.ReadAllLines(asset.languageFile.text);
                for(int i = 0; i < _lines.Length; i++){
                    string[] _contents = _lines[i].Split("="[0]);
                    _locale.languageValues.Add(new LocaleModel.TextKeyValue() { key = _contents[0], value = _contents[1] });
                }

                /* Return Model with Parsed Values */
                _locale.languageFile = null; // Remove Text Asset
                return _locale;
            }

            /* Unknown Storage Type */
            return null;
        }
    }
}
