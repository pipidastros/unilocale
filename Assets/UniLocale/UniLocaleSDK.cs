﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale General SDK Class
//=====================================================
namespace UniLocale{
    /**
     * UniLocaleSDK
     * @extends MonoBehaviour
     */
    [AddComponentMenu("UniLocale/SDK Manager")]
    public class UniLocaleSDK : MonoBehaviour{
        /* Setup SDK Instance */
        [HideInInspector] public static UniLocaleSDK Instance { get { return instance; } }      // Get Instance of SDK
        [HideInInspector] private static UniLocaleSDK instance = null;                          // SDK Instance Reference

        /* General SDK Settings */
        [Header("General SDK Settings")]
        [Tooltip("In debug mode SDK sends log to Unity Console")]
        public bool isDebugMode = false;                // Setup Debug Mode
        [Tooltip("Choose which file format you want to work with localization.")]
        public UniStorageMode currentFilesMode;         // Setup Current Localization Files Mode

        /* Locales SDK Settings */
        [Header("Locales Settings")]
        [Tooltip("Default Language Index")]
        public int defaultLocaleIndex;                  // Default Locale Code
        [Tooltip("Setup Your Localization Files Here")]
        public List<LocaleModel> localesData;           // Setup Locales Data

        /* Automatic User Data Processing */
        [Header("User Data Processing Settings")]
        [Tooltip("Do you want to detect user localization Automatically at first launch?")]
        public bool autodetectUserLanguage = true;      // Automatic Detection of User Language
        [Tooltip("Do you want to save current User Localization automatically?")]
        public bool autosaveUserSettings = true;        // Automatic Save / Load curren Language of User
        public string autosaveSettingsPath = "/locale.conf";    // Autosave Data Path

        /* User Data */
        protected UserData _userData;                           // User Data
        protected bool _isInitialized = false;                  // Is SDK Initialized?

        /* SDK Events */
        public delegate void SDKInitialized();                  // SDK Initialization Delegate
        public event SDKInitialized OnSDKInitialized;           // SDK Initialization Event
        public delegate void LanguageSwitched();                // SDK Language Change Delegate
        public event LanguageSwitched OnLanguageChanged;        // SDK Language Change Event

        #region General Initialization Methods
        /**
         * Initialize Before Scene Started
         */
        private void Awake(){
            /* Check SDK Instance. It's a realisation of Singleton */
            if (instance == null){
                instance = this;
            } else if (instance == this){
                Destroy(gameObject);
            }

            /*
             * Cant destroy this object when load another
             * scene for singleton working state
             */
            DontDestroyOnLoad(gameObject); // Don't Destroy Gamebase

            /* Parse All Locales Data */
            _parseAllLocales();

            /* Load User Data */
            LoadUserData();
        }

        /**
         * Write log only in Debug Mode
         * @params string logMessage Log Message
         */
        public void Log(string logMessage){
            if (isDebugMode) Debug.Log(logMessage);
        }

        /**
         * Is SDK Initialized?
         * @return bool Initialization Status
         */
        public bool IsInitialized(){
            return this._isInitialized;
        }

        /**
         * Load User Data and Detect Current Language
         */
        protected void LoadUserData(){
            /* Create User Data */
            this._userData = new UserData();
            this._userData.userDefaultLanguage = defaultLocaleIndex;
            this._userData.userLanguageCode = defaultLocaleIndex;

            /* Open file with User Data */
            string _pathToData = Application.persistentDataPath + autosaveSettingsPath;
            string _userData = UniLocaleStorage.openFile(_pathToData, true);
            if (_userData != null && autosaveUserSettings){
                this._userData = JsonUtility.FromJson<UserData>(_userData);
                return;
            }

            /* Detect User Language */
            if (autodetectUserLanguage){
                /* Get Culture Data */
                CultureInfo _info = _getCurrentCultureInfo();
                string _systemLocale = _info.TwoLetterISOLanguageName;

                /* Detect Language Code */
                int _defaultLangIndex = _FindLanguageByCode(_systemLocale);
                if(_defaultLangIndex != -1){
                    this._userData.userDefaultLanguage = _defaultLangIndex;
                    this._userData.userLanguageCode = _defaultLangIndex;
                    if (autosaveUserSettings) SaveUserData();
                    return;
                }
            }

            /* Finalize Initialization */
            _isInitialized = true;
            if (OnSDKInitialized != null) OnSDKInitialized();
        }

        /**
         * Save User Data
         */
        protected void SaveUserData(){
            string _pathToData = Application.persistentDataPath + autosaveSettingsPath;
            string _userData = JsonUtility.ToJson(this._userData);
            UniLocaleStorage.saveFile(_pathToData, _userData, true);
        }

        /**
         * Get Culture Info
         * @return CultureInfo Locale Info
         */
        protected CultureInfo _getCurrentCultureInfo(){
            SystemLanguage currentLanguage = Application.systemLanguage;
            CultureInfo correspondingCultureInfo = CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(x => x.EnglishName.Equals(currentLanguage.ToString()));
            return CultureInfo.CreateSpecificCulture(correspondingCultureInfo.TwoLetterISOLanguageName);
        }
        #endregion

        #region Work With Locales
        /**
         * Get Languages List
         * @return List<string> List of Languages
         */
        public List<string> GetLanguagesList(){
            List<string> _languages = new List<string>();
            for(int i = 0; i < this.localesData.Count; i++){
                _languages.Add(localesData[i].languageName);
            }

            return _languages;
        }

        /**
         * Find Locale By Code
         * @param string code Language Code
         * @return LocaleModel Model or null
         */
        public LocaleModel FindLocaleByCode(string code){
            LocaleModel _model = null;
            for(int i = 0; i < localesData.Count; i++){
                if(localesData[i].languageCode == code){
                    _model = localesData[i];
                }
            }

            return _model;
        }

        /**
         * Find Locale By Name
         * @param string code Language Name
         * @return LocaleModel Model or null
         */
        public LocaleModel FindLocaleByName(string name){
            LocaleModel _model = null;
            for (int i = 0; i < localesData.Count; i++)
            {
                if (localesData[i].languageName == name)
                {
                    _model = localesData[i];
                }
            }

            return _model;
        }

        /**
         * Get Text for Current Language by Key Name
         * @param string keyName Key Name
         * @param string replaceValue Value for Replace {VAR} in Locale. Not Required
         * @return string Finded Value for Key or Key Name
         */
        public string GetText(string keyName, string replaceValue = ""){
            string _value = "";
            LocaleModel _current = localesData[this._userData.userLanguageCode];
            for(int i = 0; i < _current.languageValues.Count; i++){
                if (_current.languageValues[i].key == keyName) _value = _current.languageValues[i].value;
            }

            if (_value != ""){
                if (replaceValue != ""){
                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("\\{\\w+\\}");
                    _value = regex.Replace(_value, replaceValue);
                }
                return _value;
            }else{
                return keyName;
            }
        }

        /**
         * Switch Language by Code
         * @param string code Language Code
         */
        public void SwitchLanguage(string code){
            for (int i = 0; i < localesData.Count; i++){
                if (localesData[i].languageName == name){
                    _userData.userLanguageCode = i;
                    if (OnLanguageChanged != null) OnLanguageChanged();
                    if(autosaveUserSettings) SaveUserData();
                    return;
                }
            }
        }

        /**
         * Switch Language by ID
         * @param string code Language ID
         */
        public void SwitchLanguageByID(int id){
            _userData.userLanguageCode = id;
            if (OnLanguageChanged != null) OnLanguageChanged();
            if (autosaveUserSettings) SaveUserData();
        }

        /**
         * Get Current Language Index
         */
        public int GetCurrentLanguageIndex(){
            return _userData.userLanguageCode;
        }

        /**
         * Find Language by Code and Return Index of Language
         * @param string code Language Name
         * @return int Language Index
         */
        protected int _FindLanguageByCode(string code){
            int _index = -1;
            for (int i = 0; i < localesData.Count; i++){
                if (localesData[i].languageCode == code){
                    _index = i;
                }
            }

            return _index;
        }

        /**
         * Parse Values of All Locales
         */
        protected void _parseAllLocales(){
            for(int i = 0; i < localesData.Count; i++){
                UniLocaleStorage.parseLocaleModel(localesData[i], currentFilesMode);
            }
        }
        #endregion
    }
}
 