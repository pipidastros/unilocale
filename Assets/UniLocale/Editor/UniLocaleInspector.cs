﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Inspector Class
//=====================================================
namespace UniLocale{
    /**
     * Editor Window for UniLocale SDK
     */
    [CustomEditor(typeof(UniLocaleSDK))]
    public class UniLocaleInspector : Editor{
        private static readonly string[] _dontIncludeMe = new string[] { "m_Script" };

        /**
         * Draw Inspector GUI
         */
        public override void OnInspectorGUI(){
            // Draw Ocugine Buttons
            GUILayout.Space(10f);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Show Documentation")){
                Application.OpenURL("https://gitlab.com/pipidastros/unilocale/-/blob/master/README.md");
            }
            if (GUILayout.Button("Contact with Developer"))
            {
                Application.OpenURL("mailto:iliya-sdt@yandex.ru");
            }
            GUILayout.EndHorizontal();
            

            // Draw Header
            GUILayout.Space(10f);
            GUILayout.Label("Setup Your SDK:", EditorStyles.largeLabel);

            // Draw Object Params
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
            serializedObject.ApplyModifiedProperties();
        }
    }

}