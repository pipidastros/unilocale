﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Initializer Class
//=====================================================
namespace UniLocale{
    /**
     * UniLocale Initializer Class
     * @extends EditorWindow
     */
    public class UtiLocaleInitializer : EditorWindow{
        [MenuItem("UniLocale/Setup SDK")]
        static void initializeSDKObject(){
            UniLocaleSDK _uniLocale = GameObject.FindObjectOfType<UniLocaleSDK>(true);
            if(_uniLocale != null){
                _uniLocale.transform.transform.SetSiblingIndex(0);
                Selection.activeGameObject = _uniLocale.transform.gameObject;
            }else{
                GameObject _sdk = new GameObject("UniLocale_Manager");
                _sdk.transform.SetSiblingIndex(0);
                _sdk.AddComponent<UniLocaleSDK>();
                Selection.activeGameObject = _sdk;
            }
        }

        [MenuItem("UniLocale/Show Documentation")]
        static void showDocs(){
            Application.OpenURL("https://gitlab.com/pipidastros/unilocale/-/blob/master/README.md");
        }

        [MenuItem("UniLocale/Contact with Developer")]
        static void showDashboard(){
            Application.OpenURL("mailto:iliya-sdt@yandex.ru");
        }
    }
}