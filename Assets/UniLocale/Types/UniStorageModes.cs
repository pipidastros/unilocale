﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Storage Modes Enum
//=====================================================
namespace UniLocale {
    /**
     * UniStorageMode Enum
     */
    public enum UniStorageMode{
        XMLFiles, JSONFiles, MultilineText
    }
}
