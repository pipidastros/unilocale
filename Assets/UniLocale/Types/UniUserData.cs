﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Locales Model
//=====================================================
namespace UniLocale{
    /**
     * User Data Serializable Class
     */
    [System.Serializable]
    public class UserData{
        public int userLanguageCode = 0;      // User Language Index
        public int userDefaultLanguage = 0;   // User Default Language Index from Device Settings
    }
}