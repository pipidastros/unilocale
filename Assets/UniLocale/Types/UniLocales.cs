﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale Locales Model
//=====================================================
namespace UniLocale{
    /**
     * LocaleModel Serializable Class
     */
    [System.Serializable]
    public class LocaleModel{
        public string languageName = "English";                                 // Language Name
        public string languageCode = "en";                                      // Language Code
        public TextAsset languageFile;                                          // Language File
        public List<TextKeyValue> languageValues = new List<TextKeyValue>();    // Parsed Values

        [System.Serializable]
        public class TextKeyValue{
            public string key; // Key
            public string value; // Value
        }
    }
}