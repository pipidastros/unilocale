﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  UniLocale UI Text Component Class
//=====================================================
namespace UniLocale{
    /**
     * Localized Text Component
     * @extends MonoBehaviour
     */
    [RequireComponent(typeof(Text))]
    [AddComponentMenu("UniLocale/UI/Text UI Auto Locale")]
    public class LocalizedUIText : MonoBehaviour{
        /* Component Settings */
        [Tooltip("Your Locale Key Name")]
        public string keyName = "";             // Key Name
        [Tooltip("Place here Replaceble text if Locale has {VAR}")]
        public string variableReplace = "";     // {VAR} replacer

        /* Component Reference */
        protected Text _component;
        protected bool _isHandlerAdded = false;
        protected bool _isInitialized = false;

        /**
         * Before Scene Initialized
         */
        private void Awake()
        {
            _component = this.GetComponent<Text>();
        }

        /**
         * Start is called before the first frame update
         */
        private void Start(){
            UpdateText();
            if (!_isHandlerAdded) UniLocaleSDK.Instance.OnLanguageChanged += UpdateText;
            _isInitialized = true;
        }

        /**
         * On Destroy - Disable Event Handler
         */
        private void OnDestroy()
        {
            if (_isHandlerAdded) UniLocaleSDK.Instance.OnLanguageChanged -= UpdateText;
        }

        /**
         * On Enable - Add Event Handler
         */
        private void OnEnable(){
            if (!_isHandlerAdded && _isInitialized) UniLocaleSDK.Instance.OnLanguageChanged += UpdateText;
        }

        /**
         * On Disable - Disable Event Handler
         */
        private void OnDisable()
        {
            if (_isHandlerAdded) UniLocaleSDK.Instance.OnLanguageChanged -= UpdateText;
        }

        /**
         * Update is called once per frame
         */
        public void UpdateText(){
            string _value = UniLocaleSDK.Instance.GetText(keyName, variableReplace);
            _component.text = _value;
        }
    }
}
