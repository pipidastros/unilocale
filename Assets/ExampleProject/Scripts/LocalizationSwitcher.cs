﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniLocale;

//=====================================================
//  UniLocale SDK
//  This SDK allows easy to work with Localization
//  based on XML / JSON Files
//
//  @developer              Ilya Rastorguev
//  @version                1.0.2
//  @build                  1021
//  @url                    https://vk.com/devking
//=====================================================
//=====================================================
//  Localization Switcher Component
//=====================================================
[RequireComponent(typeof(Dropdown))]
public class LocalizationSwitcher : MonoBehaviour {
    /* Dropdown Component */
    protected Dropdown _component;

    /**
     * Start is called before the first frame update
     */
    private void Start(){
        /* Get Component */
        _component = this.gameObject.GetComponent<Dropdown>();
        _component.ClearOptions();

        /* Load Locales */
        List<string> locales = UniLocaleSDK.Instance.GetLanguagesList();
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        for(int i = 0; i < locales.Count; i++){
            Dropdown.OptionData _data = new Dropdown.OptionData();
            _data.text = locales[i];
            options.Add(_data);
        }
        _component.AddOptions(options);

        /* Set Current Language Index */
        _component.value = UniLocaleSDK.Instance.GetCurrentLanguageIndex();
    }

    /**
     * Switch Language
     */
    public void SwitchLanguage(){
        UniLocaleSDK.Instance.SwitchLanguageByID(_component.value);
    }
}
