# Welcome to UniLocale SDK!
This is a simple SDK to work with localization of your projects on Unity.<br/> 
Its advantages are speed, ease of setup and support for several file types.

## License
This project is developed and supported under the MIT (Open-Source) license. You can use it as you want, but remember that it is provided "as is".

**Developer:** Ilya Rastorguev (https://vk.com/devking)

## Installation
1) **Clone** this project to your Unity Project Directory **or Download and Import** ready to use UnityPackage from this Link.
2) Press **"UniLocale"=>"Setup SDK"** in the top of Unity3D Window **OR** Create root Object and Add Component **"UniLocale"=>"SDK Manager"**
3) Done! Now you can configure your UniLocale Instance.

**Warning!**<br/>
Please make sure that UniLocale is loaded before your project scripts. Check the **ScriptExecutionOrder**.

## Get Started / Configuration
Now you need setup your SDK. **Open UniLocale_Manager instance** and **edit this parameters** in inspector:<br/>
1) In The **localesData** section, please add languages and **connect text files** from your assets.
2) Set **defaultLocaleIndex** at **"0"** or your Locale Index from step #1
3) Select your Localization files Extension from **currentFilesMode**.
4) Done! Now you can use UniLocale SDK

## All SDK Params

| Parameter  | Type | Usage |
| ------------- | ------------- | ------------- |
| **isDebugMode**  | _bool_  | Enable Debug Mode?  |
| **currentFilesMode** | _UniStorageMode (enum)_  | Localization Files Extension |
| **defaultLocaleIndex** | _int_ | Default Localization Index |
| **localesData** | _List\<LocaleModel\>_ | Localization Data and Files |
| **autodetectUserLanguage** | _bool_ | Detect user language automatically? |
| **autosaveUserSettings** | _bool_ | Save Localization Settings Automatically? |
| **autosaveSettingsPath** | _string_ | Path to store Locale Settings. Based on Application.persistentDataPath |

## Get Localized String
So, now you can get Localized String:
```csharp
UniLocaleSDK.Instance.GetText("YOUR_KEY_NAME", "VARIABLE_REPLACE");
```
**You can store in your locale value {VAR}** and replace it by **second argument of UniLocaleSDK.Instance.GetText**

## Automatic Localization for UGUI Text and TextMeshPro Text
You can add Components to set automatically value to your **UGUI Text component** or **TextMeshPro Text Component**.<br/>
**Just add "UniLocale"=>"UI"=>"Text UI Auto Locale"** Component **in the same object where the component with text is located** and adjust it.

## SDK Reference
Here you can see SDK Reference:

### General SDK Class (UniLocaleSDK)

|  Method  | Type | Arguments |  Usage |
| ------------- | ------------- | ------------- | ------------- |
| **Log** | void | **string** Log Message | Write Log Message to Console (Only in SDK Debug Mode) |
| **IsInitialized** | bool | - | Check if SDK is Initialized |
| **GetLanguagesList** | List\<string\> | - | Returns List of Available Languages |
| **FindLocaleByCode** | LocaleModel | **string** Language Code | Returns Language data by Code or Null |
| **FindLocaleByName** | LocaleModel | **string** Language Name | Returns Language data by Name or Null |
| **GetText** | string | **string** Key<br/>**string** Replace Variable Value | Returns Locale Value by Key or Key Name if not found. Replace in Value {VAR} to replaceValue |
| **SwitchLanguage** | void | **string** Lang Code | Switch Current User Language by Language Code |
| **SwitchLanguageByID** | void | **int** Lang ID | Switch Current User Language by Language ID |
| **GetCurrentLanguageIndex** | int | - | Returns Current Language Index |

### All Classes

|  Class  | Type |  Usage  |
| ------------- | ------------- | ------------- |
| **UniLocaleSDK** | Singleton | General SDK Class. You can get access to Singleton by reference **UniLocaleSDK.Instance;** |
| **UniLocaleStorage** | Static Class | Files Management Utils |
| **LocalizedUIText** | Component | UGUI Text Auto Localization Component |
| **LocalizedUITextMesh** | Component | TextMeshPro Text Auto Localization Component |
| **UniLocaleInitializer** | Editor Class | Editor Initializer Class |
| **UniLocaleInspector** | Editor Class | Custon Inspector for Singleton class |
| **LocaleModel** | Serializable Class | Languages Model |
| **LocaleModel.TextKeyValue** | Serializable Class | Subclass for list in Languages Model |
| **UniStorageMode** | enum | enum for Files Extensions. Used in UniLocaleSDK and UniLocaleStorage classes |
| **UserData** | Serializable Class | Used for storing user settings of SDK |